#!/bin/bash

# Check if ~/.local/bin exists, if not, create it
if [ ! -d ~/.local/bin ]; then
  mkdir -p ~/.local/bin
  echo "~/.local/bin directory created."
fi

# Copy all .sh files to ~/.local/bin, excluding install.sh and uninstall.sh
for file in *.sh; do
  if [[ $file != "install.sh" && $file != "uninstall.sh" && $file != *_test.sh ]]; then
    cp $file ~/.local/bin/
    chmod +x ~/.local/bin/$file
    echo "Copied $file to ~/.local/bin and made it executable."
  fi
done
echo "Remember to rehash after adding new utilities or restart your shell"

# Check if ~/.local/bin is in the PATH
if [[ $PATH != *~/.local/bin* && $PATH != *"$HOME/.local/bin"* ]]; then
  echo "It seems that ~/.local/bin is not in your PATH."
  echo "To add it, you can add the following line to your shell initialization script:"

  case $SHELL in
  /bin/sh|/bin/dash)
    echo 'export PATH="$PATH:'$HOME/.local/bin'"' ">> ~/.profile"
    ;;
  /bin/bash)
    echo 'export PATH="$PATH:'$HOME/.local/bin'"' ">> ~/.bashrc"
    ;;
  /bin/csh|/bin/tcsh)
    echo 'set path = ( $path '$HOME/.local/bin' )' ">> ~/.cshrc"
    ;;
  /bin/ksh)
    echo 'export PATH="$PATH:'$HOME/.local/bin'"' ">> ~/.kshrc"
    ;;
  /bin/zsh)
    echo 'export PATH="$PATH:'$HOME/.local/bin'"' ">> ~/.zshrc"
    ;;
  /usr/bin/fish)
    echo 'set -U fish_user_paths '$HOME/.local/bin' $fish_user_paths' ">> ~/.config/fish/config.fish"
    ;;
  *)
    echo "Unknown shell. Please manually add ~/.local/bin to your PATH."
    ;;
  esac
fi

