#!/bin/bash

# Variables to hold stack IDs and lock IDs
failed_stack_ids_list=()
lock_ids_list=()

# Fetch all stack information in JSON format using spacectl
stacks=$(spacectl stack list -o json)

# Use jq to filter and get stack IDs where state == "FAILED"
failed_stack_ids=$(echo "$stacks" | jq -r '.[] | select(.state == "FAILED") | .id')

# Loop through each failed stack ID
for stack_id in $failed_stack_ids; do
    echo "Processing stack: $stack_id"
    
    # Fetch the latest logs for the failed stack
    logs=$(spacectl stack logs --run-latest --id "$stack_id" | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g')

    # Check if the logs contain the error message about the state lock
    if echo "$logs" | grep -q "Error acquiring the state lock"; then
        # Extract the lock ID
        lock_id=$(echo "$logs" | grep -A 4 "Lock Info:" | grep "ID:" | awk '{print $NF}')
	echo "Creating unlock request for $stack_id and lock $lock_id"
	spacectl stack task --id "$stack_id" "terraform force-unlock -force $lock_id"
        
        # Append the stack ID and the corresponding lock ID to the lists
        failed_stack_ids_list+=("$stack_id")
        lock_ids_list+=("$lock_id")
    else
        echo "No lock error found for stack: $stack_id"
    fi
done

# Print out all the failed stack IDs with their corresponding lock IDs
echo "Failed stacks with lock IDs:"
for i in "${!failed_stack_ids_list[@]}"; do
    echo "Stack ID: ${failed_stack_ids_list[$i]}, Lock ID: ${lock_ids_list[$i]}"
done
