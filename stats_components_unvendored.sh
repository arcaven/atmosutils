#!/bin/bash

count=0

# Find all directories under components/terraform excluding .terraform
# Redirect output into the while loop to process in the current shell
while IFS= read -r dir; do
    # Check if main.tf exists and component.yaml does not exist in the directory
    if [[ -f "$dir/main.tf" && ! -f "$dir/component.yaml" ]]; then
        #echo "Match found in directory: $dir" # Debug line to indicate a match
        ((count++))
    fi
done < <(find components/terraform -type d -name .terraform -prune -o -type d -print)

echo "Components |  Unvendored components: $count"

