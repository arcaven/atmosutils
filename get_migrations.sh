#!/bin/bash

# Check if the required number of arguments are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <pod-name> <namespace>"
    exit 1
fi

POD_NAME=$1
KUBE_NAMESPACE=$2
DATE=$(date +%Y%m%d)
TARBALL_NAME="$NAMESPACE-clipbe-migrations-${DATE}.tgz"
TARBALL_PATH_IN_POD="/tmp/${TARBALL_NAME}"

# Create a tarball of the desired directories within the pod with the current date
kubectl -n $KUBE_NAMESPACE exec $POD_NAME -- sh -c \
  "find / -type d -name migrations -print0 | tar --null -czvf $TARBALL_PATH_IN_POD --files-from -"

# incorporate
## Find directories named 'migrations' starting from the root directory
#find / -type d -name migrations -exec sh -c '
#  # Within each 'migrations' directory found, find all files
#  find "$0" -type f |
#  # Use grep with extended regex to filter files that either:
#  # - Start with four digits followed by an underscore
#  # - Start with "__init__"
#  grep -E "(/[0-9]{4}_.*|/__init__.*)[^/]" |
#  # Exclude files in '__pycache__' directories
#  grep -v __pycache__
#' {} \;


# Check if the tarball creation was successful
if [ $? -ne 0 ]; then
    echo "Error creating tarball in the pod. Exiting."
    exit 1
fi

# Copy the tarball from the pod to the local machine
kubectl -n $KUBE_NAMESPACE cp $POD_NAME:$TARBALL_PATH_IN_POD ./$TARBALL_NAME

# Check if the copy operation was successful
if [ $? -ne 0 ]; then
    echo "Error copying tarball to local machine. Exiting."
    exit 1
fi

# Remove the tarball from the pod
kubectl -n $KUBE_NAMESPACE exec $POD_NAME -- rm $TARBALL_PATH_IN_POD

# Check if the cleanup was successful
if [ $? -ne 0 ]; then
    echo "Error removing tarball from the pod. Exiting."
    exit 1
fi

echo "Backup completed successfully! Tarball is now on your local machine with the name ${TARBALL_NAME}."

