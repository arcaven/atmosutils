# atmosutils

## Name
atmosutils - utilities for atmos

## Description
these are a series of scripts and utility tools that make maintaining infrastructure as code repositories managed with atmos easier

## Badges

## Visuals

## Installation

```sh
./install.sh
```

## Usage

```sh
acv_list.sh -s version
acv_list.sh
acv_list.sh -d -s version
acv_update.sh 1.250.6
```

## Support

## Roadmap

## Contributing

## Authors and acknowledgment

## License

## Project status
