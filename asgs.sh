#!/bin/bash

# Read terraform output from stdin
terraform_output=$(cat)

# Extract the autoscaling group name from the Terraform output
asg_name=$(echo "$terraform_output" | grep -A1 "_autoscaling_group_name" | grep -oP '"[^"]+"' | tr -d '"') || {
	echo "Failed to find an ASG in the terraform output."
	exit 1
}

# Parse namespace, tenant, environment, and stage from the ASG name
IFS='-' read -r namespace tenant environment stage asginstance <<<"$asg_name"

# Assign role
role="$namespace-$tenant-gbl-$stage-admin"
echo "Role assigned as $role"

# Monitor the instance refresh
while true; do
	refresh_status=$(AWS_PROFILE=$role aws autoscaling describe-instance-refreshes \
		--auto-scaling-group-name "$asg_name" --query "InstanceRefreshes[0].Status" --output text)

	percentage_complete=$(AWS_PROFILE=$role aws autoscaling describe-instance-refreshes \
		--auto-scaling-group-name "$asg_name" --query "InstanceRefreshes[0].PercentageComplete" --output text)

	echo "Refresh status: $refresh_status, Percentage complete: $percentage_complete%"

	if [[ "$refresh_status" == "Successful" || "$percentage_complete" -eq 100 ]]; then
		break
	fi

	sleep 5
done

# List EC2 instances in the ASG
echo "Instance refresh complete. Listing EC2 instances in the ASG:"
AWS_PROFILE=$role aws autoscaling describe-auto-scaling-instances --query \
	"AutoScalingInstances[?AutoScalingGroupName=='$asg_name'].[InstanceId,InstanceType,AvailabilityZone,LifecycleState]" --output table
