#!/bin/bash

# run only from infra repo base

echo -n "Components |     Non-component dirs: "
find components/terraform -type d -name .terraform -prune -o -type d -print | while read dir; do if [ ! -f "$dir/main.tf" ]; then echo "$dir"; fi; done | wc -l
