#!/bin/bash

# Check if the required number of arguments are provided
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <tarball> <pod-name> <namespace>"
    exit 1
fi

TARBALL=$1
POD_NAME=$2
KUBE_NAMESPACE=$3

# Copy the tarball from the local machine to the pod
kubectl -n $KUBE_NAMESPACE cp $TARBALL $POD_NAME:/clipbe-site-packages.tar.gz

# Check if the copy operation was successful
if [ $? -ne 0 ]; then
    echo "Error copying tarball to the pod. Exiting."
    exit 1
fi

# Extract the tarball inside the pod, preserving permissions and ownership
kubectl -n $KUBE_NAMESPACE exec $POD_NAME -- tar -xzvf /clipbe-site-packages.tar.gz -C /

# Check if the extraction was successful
if [ $? -ne 0 ]; then
    echo "Error extracting tarball in the pod. Exiting."
    exit 1
fi

# Remove the tarball from the pod
kubectl -n $KUBE_NAMESPACE exec $POD_NAME -- rm /clipbe-site-packages.tar.gz

# Check if the cleanup was successful
if [ $? -ne 0 ]; then
    echo "Error removing tarball from the pod. Exiting."
    exit 1
fi

echo "Migration restored successfully in the pod."


