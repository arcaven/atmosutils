#!/bin/bash

# Function to print usage information
usage() {
  echo "Usage: $0 [OPTIONS]"
  echo "Options:"
  echo "  -d              Enable debug mode"
  echo "  -o [table|csv]  Specify output format. Options are table or csv. Default is table"
  echo "  -s [key]        Specify sort key. Options are component or version. Default is component"
  echo "  -h              Display this help message and exit"
  exit 1
}

# Debug, sort, and output flags
debug=0
sort_key="component"
output_format="table"

# Process command-line options
while getopts "do:s:h" opt; do
  case ${opt} in
    d)
      debug=1
      ;;
    o)
      if [[ $OPTARG == "table" || $OPTARG == "csv" ]]; then
        output_format=$OPTARG
      else
        echo "Invalid option for -o: $OPTARG. Defaulting to table." 1>&2
        output_format="table"
      fi
      ;;
    s)
      if [[ $OPTARG == "component" || $OPTARG == "version" ]]; then
        sort_key=$OPTARG
      else
        echo "Invalid option for -s: $OPTARG. Defaulting to component." 1>&2
        sort_key="component"
      fi
      ;;
    h)
      usage
      ;;
    \?)
      echo "Invalid option: $OPTARG" 1>&2
      exit 1
      ;;
  esac
done

# Process each component.yaml file found in the components/terraform directory
while IFS= read -r file; do
  if [ "$debug" -eq 1 ]; then
    echo "Processing file: $file"
  fi

  # Extract the directory path below components/terraform
  component=$(dirname "${file#components/terraform/}")
  if [ "$debug" -eq 1 ]; then
    echo "Extracted component: $component"
  fi

  # Extract version from each file
  version=$(grep -m 1 'version: ' "$file" | awk -F': ' '{print $2}')

  # If there are multiple versions in the file, mark the component as having multiple versions
  version_count=$(grep -c 'version: ' "$file")
  if [ "$version_count" -gt 1 ]; then
    multiversion="+"
  else
    multiversion=""
  fi

  # Append component, version, and multi-version flag to results array
  results+=("$component,$version,$multiversion")
done < <(find components/terraform -type f -name component.yaml ! -path '*/.terraform/*')

# Sort results array
if [[ $sort_key == "component" ]]; then
  IFS=$'\n' results=($(sort <<<"${results[*]}"))
elif [[ $sort_key == "version" ]]; then
  IFS=$'\n' results=($(sort -t ',' -k2,2 -V <<<"${results[*]}"))
fi

# Print results
if [[ $output_format == "table" ]]; then
  printf "%-40s %-10s %-1s\n" "Component" "Version" "Multiple Versions"
  for result in "${results[@]}"; do
    IFS=',' read -r component version multiversion <<<"$result"
    printf "%-40s %-10s %-1s\n" "$component" "$version" "$multiversion"
  done
elif [[ $output_format == "csv" ]]; then
  echo "Component,Version,Multiple Versions"
  for result in "${results[@]}"; do
    IFS=',' read -r component version multiversion <<<"$result"
    echo "$component,$version,$multiversion"
  done
fi

