#!/bin/bash

# run only from infra repo base
echo -n "Components |    Vendored components: "
find components/terraform -type d -name .terraform -prune -o -type f -name 'component.yaml' -print | wc -l
