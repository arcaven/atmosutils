#!/bin/bash

# Initialize data structures
declare -a stacks
declare -a spaceliftdisabled
declare -a spaceliftenabled

debug=false
printspaceliftdisabled=true
printspaceliftenabled=false

# Check for debug flag
if [[ $1 == "-d" ]]; then
    debug=true
fi

# Function to process the output of "atmos describe component"
process_output() {
    local stack=$1
    local component=$2
    local output=$3

    # Check for metadata.type: abstract and skip if found
    local is_abstract=$(echo "$output" | yq e '.metadata.type' -)
    if [[ $is_abstract == "abstract" ]]; then
        [[ $debug == true ]] && echo -n "."
        return
    fi

    # Check for settings.spacelift.workspace_enabled
    local is_enabled=$(echo "$output" | yq e '.settings.spacelift.workspace_enabled' -)
    if [[ $is_enabled == "true" ]]; then
        spaceliftenabled+=("$stack/$component")
        [[ $debug == true ]] && echo -n "+"
    elif [[ $is_enabled == "false" ]]; then
        spaceliftdisabled+=("$stack/$component")
        [[ $debug == true ]] && echo -n "-"
    fi
}

# Capture all the stacks
stacks=($(atmos list stacks))
[[ $debug == true ]] && echo "Number of stacks discovered: ${#stacks[@]}"

# Loop through each stack to get components
for stack in "${stacks[@]}"; do
    mapfile -t componentArray < <(atmos list components -s "$stack" -e true)

    # Loop through each component to describe it
    for component in "${componentArray[@]}"; do
        output=$(atmos describe component "$component" -s "$stack")
        process_output "$stack" "$component" "$output"
    done

    [[ $debug == true ]] && echo -e "\nHistogram: Stack '$stack' has ${#componentArray[@]} components."
done

# Print metrics
if [[ $debug == true ]]; then
    echo -e "\n=== Metrics ==="
    echo "Total spacelift disabled: ${#spaceliftdisabled[@]}"
    echo "Total spacelift enabled: ${#spaceliftenabled[@]}"
fi

if [[ $printspaceliftdisabled == true ]]; then
# Print out stacks and components that are spacelift disabled
echo "=== Spacelift Disabled ==="
for item in "${spaceliftdisabled[@]}"; do
    echo "$item"
done
fi

if [[ $printspaceliftenabled == true ]]; then
# Print out stacks and components that are spacelift enabled
echo "=== Spacelift Enabled ==="
for item in "${spaceliftenabled[@]}"; do
    echo "$item"
done
fi

