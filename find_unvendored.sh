find ./components/terraform -type d \( -name deprecated -o -name .terraform \) -prune -o -type f -name 'main.tf' -print | while read file; do
    if [[ ! -e "$(dirname "$file")/component.yaml" ]]; then
        echo $(dirname "$file")
    fi
done | uniq

