#!/bin/bash

find $(pwd) -type d \( -name '.terraform' -o -name 'deprecated' \) -prune -o -type f -name '*.tf' -print | while read -r file; do
    relative_path=$(realpath --relative-to=$(pwd) "$file")
    awk -v path="$relative_path" '
    BEGIN {ORS="\n"}
    /^resource "[A-Za-z0-9_-]*" "[A-Za-z0-9_-]*" \{/ {
        resource_line=$0
        resource_line_number=NR
        while ((getline next_content) > 0) {
            if (next_content ~ /^[[:blank:]]*#/ || next_content ~ /^[[:blank:]]*$/) {
                continue
            }
            if (next_content !~ /count/ && next_content !~ /enabled/ && next_content !~ /for_each/) {
                print path, ":" resource_line_number, resource_line "\n" path, ":" NR, next_content
            }
            break
        }
    }' "$file";
done

