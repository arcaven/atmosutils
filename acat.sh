#!/bin/bash

# Print usage message
print_usage() {
  echo "Usage: acat.sh [-r] [-v] [files or directories]"
  echo ""
  echo "Options:"
  echo "  -r              Process directories recursively"
  echo "  -v              Verbose mode (prints ignored file names)"
  echo "  -h, --help      Show this help message"
  echo ""
  echo "Description:"
  echo "  acat.sh reads and prints the contents of the specified files or directories."
  echo "  It respects patterns defined in ~/.acatignore, similar to .gitignore."
  echo "  If a file or directory matches a pattern in ~/.acatignore, it will be ignored."
  exit 0
}

# Read patterns from .acatignore if it exists
IGNORE_FILE="$HOME/.acatignore"
IGNORE_PATTERNS=()

read_ignore_patterns() {
  local file="$1"
  ignore_patterns=()
  while read -r pattern; do
    # Skip empty lines and comments
    [[ -z "$pattern" || "$pattern" =~ ^#.* ]] && continue
    ignore_patterns+=("$pattern")
  done < "$file"
}

# Function to check if a file should be ignored
should_ignore() {
  local file="$1"
  for pattern in "${ignore_patterns[@]}"; do
    if [[ "$file" == $pattern || "$file" == */$pattern || "$file" == $pattern/* ]]; then
      return 0 # true (ignore the file)
    fi
  done
  return 1 # false (do not ignore the file)
}

# Function to handle a single file
process_file() {
  local file="$1"

  if should_ignore "$file"; then
    if [[ "$verbose" == true ]]; then
      echo "### ignoring $file"
    fi
    return
  fi

  # Print the file path as a comment and its contents
  echo "### start: $file"
  cat "$file"
  echo "### end: $file"
  echo ""
}

# Function to process directories
process_directory() {
  local dir="$1"
  local recursive="$2"

  if should_ignore "$dir"; then
    if [[ "$verbose" == true ]]; then
      echo "### ignoring $dir"
    fi
    return
  fi

  if [[ "$recursive" == true ]]; then
    # Find all files recursively
    find "$dir" -type f | while read -r file; do
      process_file "$file"
    done
  else
    # Find files only in the top directory
    find "$dir" -maxdepth 1 -type f | while read -r file; do
      process_file "$file"
    done
  fi
}

# Check for the recursive (-r) and verbose (-v) options
recursive=false
verbose=false

while getopts "rvh-:" opt; do
  case $opt in
    r) recursive=true ;;
    v) verbose=true ;;
    h) print_usage ;;
    -)
      case "${OPTARG}" in
        help) print_usage ;;
        *) echo "Invalid option: --${OPTARG}" && exit 1 ;;
      esac
      ;;
    *) echo "Invalid option: -$opt" && exit 1 ;;
  esac
done

shift $((OPTIND - 1))

# Print usage if no arguments are provided
if [[ $# -eq 0 ]]; then
  print_usage
fi

read_ignore_patterns $IGNORE_FILE

# Iterate over each argument
for arg in "$@"; do
  if [[ -d "$arg" ]]; then
    # If it's a directory, process it
    process_directory "$arg" "$recursive"
  elif [[ -f "$arg" ]]; then
    # If it's a file, process it directly
    process_file "$arg"
  else
    echo "Skipping $arg: not a file or directory"
  fi
done
