#!/bin/bash
#set -x 

# Ensure AWS CLI and jq are installed
if ! command -v aws &> /dev/null || ! command -v jq &> /dev/null; then
    echo "This script requires AWS CLI and jq."
    exit 1
fi

# Check input format
if [[ ! $1 =~ ^[0-9]{6}$ ]]; then
    echo "Usage: $0 YYYYMM"
    exit 1
fi

YEAR=${1:0:4}
MONTH=${1:4:2}
TARGET_MONTH="$YEAR-$MONTH-01"

# Root management account ID
ROOT_MGMT_ACCOUNT_ID=$(aws sts get-caller-identity --query "Account" --output text)
if [ $? -ne 0 ]; then
    echo "Failed to get root management account ID."
    exit 1
fi

# Function to get the last day of the month
get_last_day_of_month() {
    if [[ "$OSTYPE" == "darwin"* ]]; then
        # macOS
        date -v+1m -v1d -v-1d -jf "%Y-%m-%d" "$1" "+%Y-%m-%d"
    else
        # GNU/Linux and others
        date -d "$1 +1 month -1 day" "+%Y-%m-%d"
    fi
}

# Calculate the last day of the target month
END_DATE=$(get_last_day_of_month $TARGET_MONTH)

# Function to fetch and write cost data
fetch_and_write_cost_data() {
    local account_id=$1 # Could be empty for org-wide
    local output_file=$2
    local filter_string

    if [ -z "$account_id" ]; then
       return 0
    fi
    filter_string="{\"Dimensions\":{\"Key\":\"LINKED_ACCOUNT\",\"Values\":[\"$account_id\"]}}"

    # Collect and write header to the CSV file
    echo "Date,Service,Amount" > "$output_file"

    # Fetch cost and usage data
    aws ce get-cost-and-usage --time-period Start="$TARGET_MONTH",End="$END_DATE" --granularity MONTHLY --metrics "UnblendedCost" --group-by Type=DIMENSION,Key=SERVICE --filter "$filter_string" | \
    jq -r '.ResultsByTime[].Groups[] | [.Keys[0], .Metrics.UnblendedCost.Amount] | @csv' | \
    while IFS= read -r line; do
        # Write data to the CSV file
        echo "$YEAR-$MONTH,$line" >> "$output_file"
    done
}

# Directory for organization-wide costs and specific accounts
ORG_OUTPUT_DIR="costs/$ROOT_MGMT_ACCOUNT_ID"
mkdir -p "$ORG_OUTPUT_DIR"

# Fetch and write cost data for the entire organization
ORG_OUTPUT_FILE="$ORG_OUTPUT_DIR/$1.csv"
fetch_and_write_cost_data "" "$ORG_OUTPUT_FILE"

# Get a list of account IDs within the organization, including the root
ACCOUNTS=$(aws organizations list-accounts --query 'Accounts[*].Id' --output text)

for ACCOUNT_ID in $ACCOUNTS; do
    # Define the output directory and file for each account
    OUTPUT_DIR="$ORG_OUTPUT_DIR/$ACCOUNT_ID"
    OUTPUT_FILE="$OUTPUT_DIR/$1.csv"

    # Create directory if it doesn't exist
    mkdir -p "$OUTPUT_DIR"

    # Fetch and write cost data for each account
    fetch_and_write_cost_data "$ACCOUNT_ID" "$OUTPUT_FILE"
done

aws ce get-cost-and-usage \
    --time-period Start="$TARGET_MONTH",End="$END_DATE" \
    --granularity MONTHLY \
    --metrics "UnblendedCost" \
    --group-by Type=DIMENSION,Key=SERVICE | \
    jq -r '.ResultsByTime[].Groups[] | [.Keys[0], .Metrics.UnblendedCost.Amount] | @csv' | \
    while IFS= read -r line; do
        # Write data to the CSV file
        echo "$YEAR-$MONTH,$line" >> "$ORG_OUTPUT_DIR/$YEAR$MONTH.csv"
    done

echo "Cost usage reports generated."

