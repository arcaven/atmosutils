#!/bin/bash

# Check if associative arrays are supported
bash_version=$(bash --version | grep 'GNU bash' | awk '{print $4}' | awk -F. '{print $1}')
if [ "$bash_version" -ge 4 ]; then
    declare -A remoteStateData
fi

# Create a dedicated temp directory
temp_dir="./temp_remote_states"
mkdir -p $temp_dir

# Find all remote-state.tf files under the components/terraform/ directory
find components/terraform/ -type f -name 'remote-state.tf' | while read -r filePath; do

  # Extract the directory path from components/terraform/ until the remote-state.tf file
  dirPath=${filePath#"components/terraform/"}
  dirPath=${dirPath%"/remote-state.tf"}
  
  # Sanitize dirPath for using as key
  sanitizedDirPath=$(echo "$dirPath" | tr -s '/' '_')
  
  # Initialize an empty string to hold module info for this directory
  moduleInfo=""

  # Parse the file to find module blocks and their source attributes
  awk '/module /{flag=1} /}/{flag=0} flag' "$filePath" | while read -r line; do
    if [[ "$line" =~ module\ \"([^\"]+)\" ]]; then
      moduleName=${BASH_REMATCH[1]}
    elif [[ "$line" =~ source\ =\ \"([^\"]+)\" ]]; then
      moduleSource=${BASH_REMATCH[1]}
      moduleInfo+="$moduleName:$moduleSource,"
    elif [[ "$line" =~ something_component_name\ =\ \"([^\"]+)\" ]]; then
      componentName=${BASH_REMATCH[1]}
      moduleInfo+="$moduleName:$moduleSource:$componentName,"
    fi
  done

  # Add the module information to the associative array or file
  if [ "$bash_version" -ge 4 ]; then
    remoteStateData["$sanitizedDirPath"]=$moduleInfo
  else
    echo "$moduleInfo" > "$temp_dir/$sanitizedDirPath.txt"
  fi

done

# Display the data structure
if [ "$bash_version" -ge 4 ]; then
  for dir in "${!remoteStateData[@]}"; do
    echo "$dir: ${remoteStateData[$dir]}"
  done
else
  for file in "$temp_dir"/*.txt; do
    echo "$file: $(cat $file)"
  done
fi

# Remove the temp directory if associative arrays are not supported
if [ "$bash_version" -lt 4 ]; then
  rm -rf $temp_dir
fi

