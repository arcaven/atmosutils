#!/bin/bash

# List of git repo local working copies associated with clip
directories=(
    "datamodels"
    "kafka-connector-jirasm"
    "kafka-connector-swimlane"
    "clip-plugin-apiauth"
    "clip-plugin-cogreg"
    "clip-plugin-tickets"
    "clipbe"
    "clip"
)

branches=("main" "develop")
##branches=("develop")

dependancy_files=(
	"base.txt"
	"dev.txt"
	"prod.txt"
)

base_dependencies=(
	"django"
	"django-allauth"
	"datamodels"
	"trf-spectacular"
	"pydantic"
	"Pillow"
	"uvicorn"
)

# not used yet
dev_dependencies=(
	"tox"
)

prod_dependencies=(
	"gunicorn"
	"boto3"
)

# Arrays to store output data
dir_data=()
branch_data=()
version_data=()
dependency_data=()

# Function to get the version from version.py
get_version() {
    find . -type f -name 'version.py' -exec grep -E '^VERSION\s*=\s*"[0-9]+\.[0-9]+\.[0-9]+(-dev[0-9]+)?"' {} \; |
    sed -E 's/.*"([0-9]+\.[0-9]+\.[0-9]+(-dev[0-9]+)?)".*/\1/' |
    head -n 1
}

# Function to get dependency versions from requirements/base.txt
get_dependency_versions() {
    local dep_versions=""
    for dep in "${base_dependencies[@]}"; do
        # Extract the line for the dependency, trim spaces, and then cut before any comment
        local line=$(grep -E "^$dep==[0-9]+\.[0-9]+\.[0-9]+" requirements/base.txt | sed 's/[[:space:]]*#.*//' | tr -d '[:space:]')
        # Extract the version number
        local version=$(echo "$line" | cut -d'=' -f3)
        [[ -z "$version" ]] && version="null"
        dep_versions+="$dep=$version "
    done
    echo "$dep_versions"
}

# Function to process each directory and collect data
process_directory() {
    local dir=$1
    local branch=$2

    if [[ -d "$dir" ]]; then
        cd "$dir" || return
        git checkout "$branch" > /dev/null 2>&1 || { echo "Failed to checkout $branch in $dir"; return; }
        git pull > /dev/null 2>&1 || { echo "Failed to pull in $dir"; return; }

        local version=$(get_version)
        local dependency_versions=""
        if [[ -r "requirements/base.txt" ]]; then
            dependency_versions=$(get_dependency_versions)
        fi

        # Append data to arrays
        dir_data+=("$dir")
        branch_data+=("$branch")
        version_data+=("$version")
        dependency_data+=("$dependency_versions")
        cd - > /dev/null
    else
        echo "Directory $dir does not exist."
    fi
}

# Collect data
for branch in "${branches[@]}"; do
    for dir in "${directories[@]}"; do
        process_directory "$dir" "$branch"
    done
done

# Function to calculate max width for each column
calc_max_width() {
    local max_len=0
    for item in "$@"; do
        [[ ${#item} -gt $max_len ]] && max_len=${#item}
    done
    echo $max_len
}

# Calculate max width for each column
max_dir_width=$(calc_max_width "${dir_data[@]}")
max_branch_width=$(calc_max_width "${branch_data[@]}")
max_version_width=$(calc_max_width "${version_data[@]}")
max_dep_width=$(calc_max_width "${dependency_data[@]}")

# Function to print formatted table
print_table() {
    local format="%-${max_dir_width}s %-${max_branch_width}s %-${max_version_width}s %-${max_dep_width}s\n"
    printf "$format" "Directory" "Branch" "Version" "Dependencies"
    printf "$format" "---------" "------" "-------" "------------"
    for i in "${!dir_data[@]}"; do
        printf "$format" "${dir_data[i]}" "${branch_data[i]}" "${version_data[i]}" "${dependency_data[i]}"
    done
}

# Print the table
print_table

