#!/bin/bash

# Define the log file path
LOG_FILE="command_log.log"

# Check if clusters.txt file exists
if [ ! -f clusters.txt ]; then
    echo "clusters.txt file not found!"
    exit 1
fi

# Loop through each line in clusters.txt
while IFS= read -r line; do
    # Use expect to handle the interactive command
    expect -c "
        set timeout -1
        spawn atmos describe k8s-upgrade eks/cluster -s $line
        expect {
            \"Press Enter to continue ...\" {
                send \"\r\"
                exp_continue
            }
            eof
        }
    " >> "$LOG_FILE" 2>&1
done < clusters.txt

