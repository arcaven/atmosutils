#!/bin/bash

# run only from infra repo base
echo -n "Components |       Total components: "
find components/terraform -type d -name .terraform -prune -o -type f -name 'main.tf' -print | wc -l
