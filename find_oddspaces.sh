#!/bin/bash

# Check that a directory has been provided
if [ $# -ne 1 ]; then
  echo "Usage: $0 directory"
  exit 1
fi

# Recursive function
process_files() {
  for entry in "$1"/*; do
    if [ -d "$entry" ]; then
      process_files "$entry"
    elif [ -f "$entry" ] && [[ $entry == *.yaml ]]; then
      local line_number=0
      while IFS= read -r line; do
        ((line_number++))
        # Trim all trailing characters starting from first non-space character
        leading_spaces=$(echo "$line" | sed 's/\([^ ]\).*//')
        space_count=${#leading_spaces}
        if (( space_count % 2 != 0 )); then
          echo "${entry}:${line_number},{${space_count}} Odd leading spaces"
          echo "$line"
          echo
        fi
      done < "$entry"
    fi
  done
}

# Call the function with provided directory
process_files "$1"

