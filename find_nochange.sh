#!/bin/bash

# Create temporary files
tmpfile_all_dirs=$(mktemp /tmp/all_dirs.XXXXXX)
tmpfile_one_file=$(mktemp /tmp/one_file.XXXXXX)

# Loop over all changed files
while IFS= read -r file; do
    # Extract the directory name
    dir=$(dirname "$file")

    # Add the directory name to the tmpfile_all_dirs file
    echo "$dir" >> "$tmpfile_all_dirs"
done < <(git diff --name-only)

# Now check each directory to see if it only has one changed file and it's the component.yaml file
for dir in $(sort "$tmpfile_all_dirs" | uniq); do
    num_files=$(grep -c "^$dir$" "$tmpfile_all_dirs")
    if [[ $num_files -eq 1 && $(basename $(grep "^$dir$" "$tmpfile_all_dirs")) == "component.yaml" ]]; then
        echo "$dir" >> "$tmpfile_one_file"
    fi
done

# Print the directories where only component.yaml has changed
cat "$tmpfile_one_file"

# Clean up the temporary files
rm "$tmpfile_all_dirs"
rm "$tmpfile_one_file"

