#!/bin/bash

# Check if the version argument is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <version>"
  exit 1
fi

# Store the version argument
version="$1"

# Run the alias command and process each line
grep version: $(grep -re terraform-aws-components.git components | awk -F : '{ print $1 }') | while IFS=: read -r file line; do
  sed -i '' "s/version:.*/version: $version/" "$file"
done

