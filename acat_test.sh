#!/bin/bash

# Create the acat_test directory
mkdir -p acat_test/subdir1
mkdir -p acat_test/subdir2
mkdir -p acat_test/.git
mkdir -p acat_test/.ssh

# Create some test files
echo "File in root directory" > acat_test/file1.txt
echo "Another file in root" > acat_test/file2.txt
echo "This file is in subdir1" > acat_test/subdir1/file3.txt
echo "This file is in subdir2" > acat_test/subdir2/file4.txt
echo "Should be ignored by .acatignore" > acat_test/.git/config
echo "Should also be ignored" > acat_test/.ssh/id_rsa

# Create the .acatignore file
cat <<EOL > acat_test/.acatignore
# Ignore git and ssh directories
.git/
.ssh/
EOL

# Create the expected output file
cat <<EOL > acat_test/acat_test_results.txt
### start: ./file1.txt
File in root directory
### end: ./file1.txt

### start: ./file2.txt
Another file in root
### end: ./file2.txt

### start: ./subdir1/file3.txt
This file is in subdir1
### end: ./subdir1/file3.txt

### start: ./subdir2/file4.txt
This file is in subdir2
### end: ./subdir2/file4.txt

### ignoring ./.git/config
### ignoring ./.ssh/id_rsa
EOL

echo "Test setup complete. You can now 'cd acat_test' and run '../acat.sh -v .' to validate the output."
