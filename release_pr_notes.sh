#!/bin/bash

# Set the branches
DEVELOP_BRANCH="develop"
MAIN_BRANCH="main"

# Set the output file for the release notes
OUTPUT_FILE="release_notes_v3.txt"

# Clear the output file if it already exists
> "$OUTPUT_FILE"

# Function to fetch and format PR details
fetch_pr_details() {
    PR_NUMBER=$1
    echo -e "PR #$PR_NUMBER: ${PR_DETAILS[$PR_NUMBER]}\n" >> "$OUTPUT_FILE"
}

# Fetch all merged PRs into an associative array with PR number as key
declare -A PR_DETAILS
for BRANCH in $DEVELOP_BRANCH $MAIN_BRANCH; do
    # Fetch PR details for the branch
    PRS=$(gh pr list --state merged --base "$BRANCH" --json number,title,author,mergedAt,body -q '.[] | "\(.number)=\(.title) by \(.author.login) merged at \(.mergedAt)\n\(.body)\n"')
    # Read each PR detail into the associative array
    while IFS="=" read -r PR_NUMBER DETAILS; do
        PR_DETAILS[$PR_NUMBER]=$DETAILS
    done <<< "$PRS"
done

# Get commits unique to develop but not in main
UNIQUE_COMMITS=$(git log $MAIN_BRANCH..$DEVELOP_BRANCH --oneline)

# Attempt to associate each unique commit with a PR and fetch details
for COMMIT in $UNIQUE_COMMITS; do
    COMMIT_HASH=$(echo $COMMIT | awk '{print $1}')
    # Attempt to find a PR number associated with the commit hash
    PR_NUMBER=$(gh pr list --state merged --json number,mergeCommit -q ".[] | select(.mergeCommit.oid | startswith(\"$COMMIT_HASH\")) | .number")
    if [ ! -z "$PR_NUMBER" ] && [ ${PR_DETAILS[$PR_NUMBER]+_} ]; then
        fetch_pr_details "$PR_NUMBER"
    fi
done

echo "Release notes generated in $OUTPUT_FILE"

